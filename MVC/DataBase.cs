﻿using Microsoft.EntityFrameworkCore;
using MVC.Repositories;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.IO;

namespace MVC
{
    public class DataBase : IDataBase
    {
        private readonly ApplicationContext contexto;
        private readonly IProdutoRepository produtoRepository;
        public DataBase(ApplicationContext contexto, IProdutoRepository produtoRepository)
        {
            this.contexto = contexto;
            this.produtoRepository = produtoRepository;
        }

        public void IniciarDB()
        {
            contexto.Database.EnsureCreated();
            //contexto.Database.Migrate();
            List<Livros> livros = GetLivros();
            produtoRepository.SaveProdutos(livros);
        }
        private static List<Livros> GetLivros()
        {
            var json = File.ReadAllText("livros.json");
            var livros = JsonConvert.DeserializeObject<List<Livros>>(json);
            return livros;
        }
    }
}
