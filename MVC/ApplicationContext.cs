﻿using JetBrains.Annotations;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MVC
{
    public class ApplicationContext : DbContext
    {
        public ApplicationContext(DbContextOptions options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Produto>().HasKey(k => k.Id);

            modelBuilder.Entity<Pedido>().HasKey(k => k.Id);
            modelBuilder.Entity<Pedido>().HasMany(i => i.Itens).WithOne(i => i.Pedido);
            //modelBuilder.Entity<Pedido>().HasOne(c => c.Cadastro).WithOne(c => c.Pedido).IsRequired();
            modelBuilder.Entity<Pedido>().HasOne(c => c.Cadastro).WithOne(c => c.Pedido).HasForeignKey<Pedido>(fk => fk.CadastroFk).IsRequired();

            modelBuilder.Entity<Cadastro>().HasKey(k => k.Id);
            modelBuilder.Entity<Cadastro>().HasOne(p => p.Pedido);

            modelBuilder.Entity<ItemPedido>().HasKey(k => k.Id);
            modelBuilder.Entity<ItemPedido>().HasOne(p => p.Pedido);
            modelBuilder.Entity<ItemPedido>().HasOne(p => p.Produto);

        }
    }
}
