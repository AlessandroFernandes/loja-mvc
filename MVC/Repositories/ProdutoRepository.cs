﻿using Microsoft.EntityFrameworkCore;
using MVC.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MVC.Repositories
{
    public class ProdutoRepository : BaseRepository<Produto>, IProdutoRepository
    {
        public ProdutoRepository(ApplicationContext contexto) : base(contexto)
        {
        }

        public IList<Produto> GetProduto()
        {
            return contexto.Set<Produto>().ToList();
        }

        public void SaveProdutos(List<Livros> livros)
        {
            foreach (var livro in livros)
            {
                if(!dbSet.Where(i => i.Codigo == livro.Codigo).Any())
                {
                  
                    dbSet.Add(new Produto(livro.Codigo, livro.Nome, livro.Preco));

                }
            }
            contexto.SaveChanges();
        }
    }
}
