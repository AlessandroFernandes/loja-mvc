﻿using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using MVC.Controllers;
using MVC.Repositories.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MVC.Repositories
{
    public class PedidoRepository : BaseRepository<Pedido>, IPedidoRepository
    {
        private readonly IHttpContextAccessor ContextAccessor;
        public PedidoRepository(ApplicationContext contexto, IHttpContextAccessor httpContext) : base(contexto)
        {
            this.ContextAccessor = httpContext;
        }

        public void AddList(string codigo)
        {
            var produto = contexto.Set<Produto>()
                          .Where(p => p.Codigo == codigo)
                          .SingleOrDefault();

            if(produto == null)
            {
                throw new ArgumentException("Paramento não pode ser nulo");
            }

            var pedido = GetPedido();

            var itemPedido = contexto.Set<ItemPedido>()
                             .Where(p => p.Produto.Codigo == codigo
                                    && p.Pedido.Id == pedido.Id)
                             .SingleOrDefault();

            if(itemPedido == null)
            {
                itemPedido = new ItemPedido(pedido, produto, 1, produto.Preco);
                contexto.Set<ItemPedido>()
                        .Add(itemPedido);
                contexto.SaveChanges();
            }

        }

        public Pedido GetPedido()
        {
            var pedidoId = getPedidoId();
            var pedido = dbSet
                        .Include(i => i.Itens)
                        .ThenInclude(p => p.Produto)
                        .Where(p => p.Id == pedidoId)
                        .SingleOrDefault();

            if(pedido == null)
            {
                pedido = new Pedido();
                dbSet.Add(pedido);
                contexto.SaveChanges();
                setPedidoId(pedido.Id);
            }

            return pedido;
        }

        private int? getPedidoId()
        {
            return ContextAccessor.HttpContext.Session.GetInt32("PedidoId");
        }

        private void setPedidoId(int PedidoId)
        {
            ContextAccessor.HttpContext.Session.SetInt32("PedidoId", PedidoId);
        }
    }
}
