﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MVC.Repositories.Interface
{
    public interface IPedidoRepository
    {
        Pedido GetPedido();
        void AddList(string codigo);
    }
}
