﻿using System.Collections.Generic;

namespace MVC.Repositories
{
    public interface IProdutoRepository
    {
        void SaveProdutos(List<Livros> livros);

        IList<Produto> GetProduto();
    }
}