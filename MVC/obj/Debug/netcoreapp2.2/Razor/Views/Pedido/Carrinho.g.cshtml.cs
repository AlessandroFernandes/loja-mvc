#pragma checksum "C:\Users\Alessandro\Desktop\loja-mvc\MVC\Views\Pedido\Carrinho.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "751994ea5a9826f19af2be8d8807ad0c57a81ced"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Pedido_Carrinho), @"mvc.1.0.view", @"/Views/Pedido/Carrinho.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/Pedido/Carrinho.cshtml", typeof(AspNetCore.Views_Pedido_Carrinho))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "C:\Users\Alessandro\Desktop\loja-mvc\MVC\Views\_ViewImports.cshtml"
using MVC;

#line default
#line hidden
#line 2 "C:\Users\Alessandro\Desktop\loja-mvc\MVC\Views\_ViewImports.cshtml"
using MVC.Models;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"751994ea5a9826f19af2be8d8807ad0c57a81ced", @"/Views/Pedido/Carrinho.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"7d7a8f56340c239c091cff637a00cc2fdf252300", @"/Views/_ViewImports.cshtml")]
    public class Views_Pedido_Carrinho : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<IList<ItemPedido>>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("class", new global::Microsoft.AspNetCore.Html.HtmlString("btn btn-success"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-action", "Carrossel", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_2 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-action", "Resumo", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#line 1 "C:\Users\Alessandro\Desktop\loja-mvc\MVC\Views\Pedido\Carrinho.cshtml"
   
    ViewData["title"] = "Carrinho";

#line default
#line hidden
            BeginContext(69, 586, true);
            WriteLiteral(@"
<h3>Meu Carrinho</h3>

<div class=""panel panel-default"">
    <div class=""panel-heading"">

        <div class=""row"">
            <div class=""col-md-6"">
                Item
            </div>
            <div class=""col-md-2 text-center"">
                Preço Unitário
            </div>
            <div class=""col-md-2 text-center"">
                Quantidade
            </div>
            <div class=""col-md-2"">
                <span class=""pull-right"">
                    Subtotal
                </span>
            </div>
        </div>
    </div>
    <div class=""panel-body"">
");
            EndContext();
#line 29 "C:\Users\Alessandro\Desktop\loja-mvc\MVC\Views\Pedido\Carrinho.cshtml"
         foreach (var item in Model)
        {

#line default
#line hidden
            BeginContext(703, 135, true);
            WriteLiteral("        <div class=\"row row-center linha-produto\">\n            <div class=\"col-md-3\">\n                <img class=\"img-produto-carrinho\"");
            EndContext();
            BeginWriteAttribute("src", " src=\"", 838, "\"", 893, 3);
            WriteAttributeValue("", 844, "/images/produtos/large_", 844, 23, true);
#line 33 "C:\Users\Alessandro\Desktop\loja-mvc\MVC\Views\Pedido\Carrinho.cshtml"
WriteAttributeValue("", 867, item.Produto.Codigo, 867, 22, false);

#line default
#line hidden
            WriteAttributeValue("", 889, ".jpg", 889, 4, true);
            EndWriteAttribute();
            BeginContext(894, 57, true);
            WriteLiteral(" />\n            </div>\n            <div class=\"col-md-3\">");
            EndContext();
            BeginContext(953, 17, false);
#line 35 "C:\Users\Alessandro\Desktop\loja-mvc\MVC\Views\Pedido\Carrinho.cshtml"
                              Write(item.Produto.Nome);

#line default
#line hidden
            EndContext();
            BeginContext(971, 56, true);
            WriteLiteral("</div>\n            <div class=\"col-md-2 text-center\">R$ ");
            EndContext();
            BeginContext(1029, 18, false);
#line 36 "C:\Users\Alessandro\Desktop\loja-mvc\MVC\Views\Pedido\Carrinho.cshtml"
                                             Write(item.PrecoUnitario);

#line default
#line hidden
            EndContext();
            BeginContext(1048, 370, true);
            WriteLiteral(@"</div>
            <div class=""col-md-2 text-center"">
                <div class=""input-group"">
                    <span class=""input-group-btn"">
                        <button class=""btn btn-default"">
                            <span class=""glyphicon-minus""></span>
                        </button>
                    </span>
                    <input type=""text""");
            EndContext();
            BeginWriteAttribute("value", " value=\"", 1418, "\"", 1444, 1);
#line 44 "C:\Users\Alessandro\Desktop\loja-mvc\MVC\Views\Pedido\Carrinho.cshtml"
WriteAttributeValue("", 1426, item.Quantidade, 1426, 18, false);

#line default
#line hidden
            EndWriteAttribute();
            BeginContext(1445, 450, true);
            WriteLiteral(@"
                           class=""form-control text-center"" />
                    <span class=""input-group-btn"">
                        <button class=""btn btn-default"">
                            <span class=""glyphicon-plus""></span>
                        </button>
                    </span>
                </div>
            </div>
            <div class=""col-md-2"">
                R$ <span class=""pull-right"" subtotal>
                    ");
            EndContext();
            BeginContext(1897, 36, false);
#line 55 "C:\Users\Alessandro\Desktop\loja-mvc\MVC\Views\Pedido\Carrinho.cshtml"
                Write(item.PrecoUnitario * item.Quantidade);

#line default
#line hidden
            EndContext();
            BeginContext(1934, 59, true);
            WriteLiteral("\n                </span>\n            </div>\n        </div>\n");
            EndContext();
#line 59 "C:\Users\Alessandro\Desktop\loja-mvc\MVC\Views\Pedido\Carrinho.cshtml"
        }

#line default
#line hidden
            BeginContext(2003, 167, true);
            WriteLiteral("    </div>\n    <div class=\"panel-footer\">\n        <div class=\"row\">\n            <div class=\"col-md-10\">\n                <span numero-itens>\n                    Total: ");
            EndContext();
            BeginContext(2172, 13, false);
#line 65 "C:\Users\Alessandro\Desktop\loja-mvc\MVC\Views\Pedido\Carrinho.cshtml"
                       Write(Model.Count());

#line default
#line hidden
            EndContext();
            BeginContext(2186, 183, true);
            WriteLiteral("\n                    itens\n                </span>\n            </div>\n            <div class=\"col-md-2\">\n                Total: R$ <span class=\"pull-right\" total>\n                    ");
            EndContext();
            BeginContext(2371, 46, false);
#line 71 "C:\Users\Alessandro\Desktop\loja-mvc\MVC\Views\Pedido\Carrinho.cshtml"
                Write(Model.Sum(i => i.PrecoUnitario * i.Quantidade));

#line default
#line hidden
            EndContext();
            BeginContext(2418, 169, true);
            WriteLiteral("\n                </span>\n            </div>\n        </div>\n    </div>\n</div>\n\n<div class=\"row\">\n    <div class=\"col-md-12\">\n        <div class=\"pull-right\">\n            ");
            EndContext();
            BeginContext(2587, 102, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("a", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "751994ea5a9826f19af2be8d8807ad0c57a81ced9755", async() => {
                BeginContext(2637, 48, true);
                WriteLiteral("\n                Adicionar Produtos\n            ");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_0);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Action = (string)__tagHelperAttribute_1.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_1);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(2689, 13, true);
            WriteLiteral("\n            ");
            EndContext();
            BeginContext(2702, 87, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("a", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "751994ea5a9826f19af2be8d8807ad0c57a81ced11266", async() => {
                BeginContext(2749, 36, true);
                WriteLiteral("\n                Resumo\n            ");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_0);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Action = (string)__tagHelperAttribute_2.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_2);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(2789, 34, true);
            WriteLiteral("\n        </div>\n    </div>\n</div>\n");
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<IList<ItemPedido>> Html { get; private set; }
    }
}
#pragma warning restore 1591
